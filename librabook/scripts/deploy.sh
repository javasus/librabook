#!/usr/bin/env bash

mvn clean package

echo 'Copy package'

scp -i ~/.ssh/id_rsa \
    C:/librarybook/librabook/target/librabook-1.0-SNAPSHOT.jar \
    nosulko@192.168.0.104:/home/nosulko/

echo 'Restart server...'

ssh -tt -i ~/.ssh/id_rsa nosulko@192.168.0.104 << EOF

pgrep java | xargs kill -9
nohup java -jar librabook-1.0-SNAPSHOT.jar > log.txt &

EOF

echo 'Bye'