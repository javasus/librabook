package com.javasus.librabook.service;

import com.javasus.librabook.domain.Books;
import com.javasus.librabook.domain.User;
import com.javasus.librabook.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by javasus on 02.05.2020.
 */
@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    // Указываем Spring, что хотим получить какую-то переменную, которая у нас указана в application.properties
    @Value("${upload.path}")
    private String uploadPath;

    @Value("${upload.pathBook}")
    private  String uploadPathBook;

    // Search for books by parameters
    public Iterable<Books> filterBook(String filternamebook, String filter) {

        Iterable<Books> books = null;
        if (filternamebook != null && !filternamebook.isEmpty()) {
            if(filter.equals("name")) {
                books = bookRepository.findBynamebook(filternamebook);
            } else if (filter.equals("author")) {
                books = bookRepository.findByauthorbook(filternamebook);
            } else if (filter.equals("publish")) {
                books = bookRepository.findBypublishbook(filternamebook);
            } else if (filter.equals("place")) {
                books = bookRepository.findByplacebook(filternamebook);
            } else if (filter.equals("issue")) {
                books = bookRepository.findByissuebook(
                        filternamebook.equals("available") ? true : false
                );
            }
        } else {
            books = bookRepository.findAll();
        }
        return books;
    }


    // Add book
    public void addBook(String namebook, String authorbook, String publishbook, String placebook, Boolean issuebook, User user, MultipartFile file, MultipartFile fileBook) throws IOException {

        Books book = new Books(namebook, authorbook, publishbook, placebook, issuebook, user);

        // Для добавления картинки обложки книги
        if (file != null && !file.getOriginalFilename().isEmpty()) {
            File uploadDir = new File(uploadPath);

            // Если файл не существует, то мы его создаем
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            // Добавляем к имени уникальный номер
            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + file.getOriginalFilename();

            // Загружаем файл
            file.transferTo(new File(uploadPath + "/" + resultFilename));

            book.setFilename(resultFilename);
        }

        // Для добавления книги
        if (fileBook != null && !fileBook.getOriginalFilename().isEmpty()) {
            File uploadDir = new File(uploadPathBook);

            // Если файл не существует, то мы его создаем
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            // Добавляем к имени уникальный номер
            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + fileBook.getOriginalFilename();

            // Загружаем файл
            fileBook.transferTo(new File(uploadPathBook + "/" + resultFilename));

            book.setFilenameBook(resultFilename);
        }

        bookRepository.save(book);
    }

    // Admin changes book
    public void editBook(String namebook, String authorbook, String publishbook, String placebook, Boolean issuebook, Books book, User user, MultipartFile file, MultipartFile fileBook) throws IOException {

        book.setNamebook(namebook);
        book.setAuthorbook(authorbook);
        book.setPublishbook(publishbook);
        book.setPlacebook(placebook);
        book.setIssuebook(issuebook);
        book.setAuthor(user);

        // Для добавления картинки обложки книги
        // Если файл не null & no empty тогда сохраняем его у себя
        if (file != null && !file.getOriginalFilename().isEmpty()) {
            File uploadDir = new File(uploadPath);

            // Если файл не существует, то мы его создаем
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            // Добавляем к имени уникальный номер
            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + file.getOriginalFilename();

            // Загружаем файл
            file.transferTo(new File(uploadPath + "/" + resultFilename));

            book.setFilename(resultFilename);
        } else { // Если файл null or empty тогда в новый обьект класса Books записываем старый путь до файла
            // Таким образом если до внесение изменений в книгу файл обложки уже был загружен
            // и мы не при изменении не подгружаем новый файл(не обновляем обложку), то
            //останется старый файл (старая обложка).
            book.setFilename(bookRepository.findById(book.getId()).get().getFilename());
        }

        // Для добавления книги
        if (fileBook != null && !fileBook.getOriginalFilename().isEmpty()) { // Если файл не null & no empty тогда сохраняем его у себя
            File uploadDir = new File(uploadPathBook);

            // Если файл не существует, то мы его создаем
            if (!uploadDir.exists()) {
                        uploadDir.mkdir();
            }
            // Добавляем к имени уникальный номер
            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + fileBook.getOriginalFilename();

            // Загружаем файл
            fileBook.transferTo(new File(uploadPathBook + "/" + resultFilename));

            book.setFilenameBook(resultFilename);
        } else { // Если файл null or empty тогда в новый обьект класса Books записываем старый путь до файла
            // Таким образом если до внесение изменений в книгу файл обложки уже был загружен
            // и мы не при изменении не подгружаем новый файл(не обновляем обложку), то
            //останется старый файл (старая обложка).
            book.setFilenameBook(bookRepository.findById(book.getId()).get().getFilenameBook());
        }

        bookRepository.save(book);
    }

    // Admin delete book
    public Iterable<Books> deleteBook(Long id) {

        bookRepository.deleteById(id);

        Iterable<Books> books = bookRepository.findAll();

        return books;
    }
}
