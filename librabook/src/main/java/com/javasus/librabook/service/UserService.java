package com.javasus.librabook.service;

import com.javasus.librabook.domain.Role;
import com.javasus.librabook.domain.User;
import com.javasus.librabook.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by javasus on 07.02.2020.
 */
@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MailSender mailSender;

    @Autowired
    private PasswordEncoder passwordEncoder; // кодирование паролей

    //Кладем  в hostname значение зависищее от того, какой application.properties прописан в Programm arguments.
    @Value("${hostname}")
    private String hostname;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return  user;
    }

    // РЕГИСТРАЦИЯ нового пользователя
    public boolean addUser(User user){
        User userFromDb = userRepository.findByUsername(user.getUsername());

        // Проверяем: пользователь, который регистрируется, есть в базе данных?
        if (userFromDb != null) {
            return false; // если он есть в БД то возвращаем false, что означает, что пользователь не добавлен.
        }

        user.setActive(false);// пока активация по почте не пройдена активность пользователя false
        user.setRoles(Collections.singleton(Role.USER));
        user.setActivationCode(UUID.randomUUID().toString());
        user.setPassword(passwordEncoder.encode(user.getPassword())); //шифруем пароль при регистрации

        userRepository.save(user);

        sendMessage(user);

        return true; //  Если во время регистрации пользователя нет в БД, то мы его регистрируем и возвращаем true
    }


    // АКТИВАЦИЯ пользователя
    public boolean activateUser(String code) {

        User user = userRepository.findByActivationCode(code);
        // если пользователь не найден, возвращаем false.
        if (user == null) {
            return false; // активация не удалась.
        }

        user.setActivationCode(null); //пользователь подтвердил свой почтовый ящик
        user.setActive(true); // если активация удалась, то активность true

        userRepository.save(user);
        return true;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    // АДМИН меняет данные пользователя
    public void saveUser(
            User user,
            String username,
            String userlastname,
            String useradress,
            String userphonenumber,
            String password,
            Map<String, String> form
    ) {

        user.setUsername(username);
        user.setUserlastname(userlastname);
        user.setUseradress(useradress);
        user.setUserphonenumber(userphonenumber);
        // проверяем, изменил ли пользователь пароль
        if (!StringUtils.isEmpty(password)) {
            user.setPassword(passwordEncoder.encode(password)); //шифруем пароль при изменении пароля поьзователем
        }

        Set<String> roles = Arrays.stream(Role.values())
                .map(Role::name)
                .collect(Collectors.toSet());

        user.getRoles().clear();

        for (String key : form.keySet()) {
            if (roles.contains(key)) {
                user.getRoles().add(Role.valueOf(key));
            }
        }

        userRepository.save(user);
    }

    // ПОЛЬЗОВАТЕЛЬ меняет свои данные
    public void editUser(
            User user,
            String password,
            String email,
            String userlastname,
            String useradress,
            String userphonenumber
    ) {
        String userEmail = user.getEmail();

        boolean isEmailChanged = (email != null && !email.equals(userEmail)) || (userEmail != null && !userEmail.equals(email));

        if (isEmailChanged) {
            user.setEmail(email);

            if (!StringUtils.isEmpty(email)) {
                user.setActivationCode(UUID.randomUUID().toString());
            }
        }

        if (!StringUtils.isEmpty(password)) {
            user.setPassword(passwordEncoder.encode(password)); //шифруем пароль при регистрации
        }

        user.setUserlastname(userlastname);
        user.setUseradress(useradress);
        user.setUserphonenumber(userphonenumber);

        userRepository.save(user);

        if (isEmailChanged) {
            sendMessage(user);
        }
    }

    private void sendMessage(User user) {
        // реализуем отправку сообщений
        if (!StringUtils.isEmpty(user.getEmail())) {

            String message = String.format(
                    "Hello, %s!" +
                            "Welcome to Library! Please, visit next link: http://%s/activate/%s",
                    //"Welcome to Library! Please, visit next link: http://localhost:8080/activate/%s",
                    user.getUsername(),
                    hostname, //получаем сюда значение hostname в зависимости от  выбраного applicatiion.properties
                    user.getActivationCode()
            );

            mailSender.send(user.getEmail(), "Activation code", message);

        }
    }

    // АДМИН удаляет пользователя
    public void userDelete (Long id, User user) {

        userRepository.deleteById(id);
        mailSender.send(user.getEmail(), "Account deleting", "Hello, your account has been deleted from the LIBRARY application!");

    }
}
