package com.javasus.librabook.repository;

import com.javasus.librabook.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by javasus on 04.02.2020.
 */
public interface UserRepository extends JpaRepository<User,Long> {

    User findByUsername(String username);

    User findByActivationCode(String code);
}
