package com.javasus.librabook.repository;

import com.javasus.librabook.domain.Books;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by javasus on 27.01.2020.
 */


// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete
public interface BookRepository extends CrudRepository<Books, Long> {
    List<Books> findBynamebook(String namebook);
    List<Books> findByauthorbook(String authorbook);
    List<Books> findBypublishbook(String publishbook);
    List<Books> findByplacebook(String placebook);
    List<Books> findByissuebook(Boolean issuebook);

}
