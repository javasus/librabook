package com.javasus.librabook.controller;

/**
 * Created by javasus on 23.01.2020.
 */

import com.javasus.librabook.domain.Books;
import com.javasus.librabook.domain.Role;
import com.javasus.librabook.domain.User;
import com.javasus.librabook.repository.BookRepository;
import com.javasus.librabook.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;
import java.util.Set;

@Controller
public class MainController {
    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private BookService bookService;

    @GetMapping("/")
    public String library(Map<String,Object> model)
    {
        return "library";
    }

    @GetMapping("/main")
    public String main(
            @RequestParam(required = false, defaultValue = "")String filternamebook,
            @RequestParam(required = false, defaultValue = "")String filter,
            //@AuthenticationPrincipal User user,
            Model model) {

        model.addAttribute("books", bookService.filterBook(filternamebook, filter));
        model.addAttribute("filter", filternamebook);

        return "main";
    }

}
