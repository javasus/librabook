package com.javasus.librabook.controller;

import com.javasus.librabook.domain.User;
import com.javasus.librabook.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

/**
 * Created by javasus on 04.02.2020.
 */
@Controller
public class RegistrationController {

    @Autowired
    private UserService userService;

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("message","");
        return ("/registration");
    }

    @PostMapping("/registration")
    public String addUser(
            @RequestParam("password2") String passwordConfirm,
            @Valid User user,
            BindingResult bindingResult,
            Model model
    ) {
        // Проверим что passwordConfirm непустой
        boolean isConfirmEmpty = StringUtils.isEmpty(passwordConfirm);
        if (isConfirmEmpty) {
            model.addAttribute("password2Error", "Password confirmation can't be empty!");
        }
        // Проверим совпадаютли введеные пароли.Проверка пароля на корректность.
        if ( user.getPassword()!= null && !user.getPassword().equals(passwordConfirm)){
            model.addAttribute("passwordError", "Passwords are different!");
        }
        // Если bindingResult имеет ошибки, то получаем лист с ошибками, преобразуем его м map.
        if (isConfirmEmpty || bindingResult.hasErrors()) {
            Map<String, String> errorsMap = UtilsController.getErrors(bindingResult);
            model.mergeAttributes(errorsMap);
            //model.addAttribute("massage", user);
            return "registration";
        } else { // Если ошибок нет то добоваляем книгу.
            if (!userService.addUser(user)) { // Если не смогли добавть юзера, значит юзер уже существует
                model.addAttribute("usernameError", "User exists!");
                return "registration";
            }
            return "redirect:/login";
        }
    }

    // маппинг для активации пользователя по почте
    @GetMapping("/activate/{code}")
    public String activate(Model model,
                           @PathVariable String code
    ) {
        boolean isActivated = userService.activateUser(code);

        //проверим найден ли пользователь с таким кодом активации
        if (isActivated) {
            model.addAttribute("messageType", "success");
            model.addAttribute("message", "User successfully activated!");
        } else {
            model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Activated code is not found!");
        }

        return "login";
    }

}
