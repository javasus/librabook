package com.javasus.librabook.controller;

import com.javasus.librabook.domain.Books;
import com.javasus.librabook.domain.User;
import com.javasus.librabook.repository.BookRepository;
import com.javasus.librabook.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by javasus on 20.02.2020.
 */
// Клас для добавления и удаления книг, редактирования полей книг и добавления книг в электронную базу данных.
@Controller
@PreAuthorize("hasAuthority('ADMIN')") // дает допуск ко всем мапингам ниже только с ролью ADMIN
public class BookController {

    @Autowired
    private BookService bookService;

    @Autowired
    private BookRepository bookRepository;

    // Указываем Spring, что хотим получить какую-то переменную, которая у нас указана в application.properties
    @Value("${upload.path}")
    private String uploadPath;

    @Value("${upload.pathBook}")
    private  String uploadPathBook;

    // shows a list of books on the Booklist page
    @GetMapping("/book")
    public String bookList(
            @RequestParam(required = false, defaultValue = "")String filternamebook,
            @RequestParam(required = false, defaultValue = "")String filter,
            Model model) {

        model.addAttribute("books", bookService.filterBook(filternamebook,filter));
        model.addAttribute("filter", filternamebook);

        return "bookList";
    }

    // Added Book
    //@RequestMapping(method = RequestMethod.POST)
    @PostMapping ("/book")
    public String add(
            @AuthenticationPrincipal User user,
            @Valid Books book,
            BindingResult bindingResult,
            Model model,
            @RequestParam("file") MultipartFile file,
            @RequestParam("fileBook") MultipartFile fileBook

    ) throws IOException {

        // Если bindingResult имеет ошибки, то получаем лист с ошибками, преобразуем его в map.
        if (bindingResult.hasErrors()) {
            Map<String, String> errorsMap = UtilsController.getErrors(bindingResult);
            model.mergeAttributes(errorsMap);
            model.addAttribute("book",book);
        } else { // Если ошибок нет, то добоваляем книгу.
            bookService.addBook(
                    book.getNamebook(),
                    book.getAuthorbook(),
                    book.getPublishbook(),
                    book.getPlacebook(),
                    book.getIssuebook(),
                    user,
                    file,
                    fileBook
            );
        }
        Iterable<Books> books = bookRepository.findAll();
        model.addAttribute("books",books);
        model.addAttribute("filter", "");
        return "bookList";
    }

    // Get-method for edit book on the bookEdit page
    @GetMapping("/book/{book}") // ожидаем maping помимо /book через / будет идти идентификатор
    public String bookEditForm(
            @PathVariable Books book,
            Model model) {
        model.addAttribute("book", book);
        return "bookEdit";
    }

    // Admin changes book
    @PostMapping("/book/{id}")
    public String editBook(
            @AuthenticationPrincipal User user,
            @Valid Books book,
            BindingResult bindingResult,
            Model model,
            @RequestParam("file") MultipartFile file,
            @RequestParam("fileBook") MultipartFile fileBook
    ) throws IOException {

        // Если bindingResult имеет ошибки, то получаем лист с ошибками, преобразуем его в map.
        if (bindingResult.hasErrors()) {
            Map<String, String> errorsMap = UtilsController.getErrors(bindingResult);

            book.setFilename(bookRepository.findById(book.getId()).get().getFilename());
            book.setFilenameBook(bookRepository.findById(book.getId()).get().getFilenameBook());

            model.mergeAttributes(errorsMap);
            model.addAttribute("book",book);
            return "bookEdit";
        } else { // Если ошибок нет, то изменям книгу.
            bookService.editBook(
                    book.getNamebook(),
                    book.getAuthorbook(),
                    book.getPublishbook(),
                    book.getPlacebook(),
                    book.getIssuebook(),
                    book,
                    user,
                    file,
                    fileBook
            );
            return "redirect:/book";
        }



    }


    // Get-method for deletion book on the bookDelete  page
    @GetMapping("/book/delete/{book}") // ожидаем maping помимо /book через / будет идти идентификатор
    public String bookDeleteForm (
            @PathVariable Books book,
            Model model
    ) {
        model.addAttribute("book", book);
        return "bookDelete";
    }

    // Post-method for deletion book on the bookDelete page
    @PostMapping("/book/delete/{id}")
    public String delete (
            @RequestParam ("id") Long id,
            Map<String, Object> model
    ) {
        model.put("books", bookService.deleteBook(id));
        return "bookList";
    }
}
