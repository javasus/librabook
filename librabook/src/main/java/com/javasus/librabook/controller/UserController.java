package com.javasus.librabook.controller;

import com.javasus.librabook.domain.Role;
import com.javasus.librabook.domain.User;
import com.javasus.librabook.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by javasus on 12.02.2020.
 */
// Класс для редактирования ролей пользователей.
@Controller
@RequestMapping("/user") // ВСЕ методы описанные тут для страницы /user

public class UserController {
    @Autowired
    private UserService userService;

    // АДМИН просматривает данные пользователей
    @PreAuthorize("hasAuthority('ADMIN')") // дает допуск ко всем мапингам ниже только с ролью ADMIN
    @GetMapping
    public String userList(Model model) {
        model.addAttribute("users", userService.findAll());
        return "userList";
    }

    // АДМИН просматривает данные конкретного пользователя
    @PreAuthorize("hasAuthority('ADMIN')") // дает допуск ко всем мапингам ниже только с ролью ADMIN
    @GetMapping("{user}") // ожидаем maping помимо /user через / будет идти идентификатор
    public String userEditForm(
            @PathVariable User user,
            Model model) {
        model.addAttribute("user", user);
        model.addAttribute("roles", Role.values());
        return "userEdit";
    }

    // АДМИН меняет данные пользователя
    @PreAuthorize("hasAuthority('ADMIN')") // дает допуск ко всем мапингам ниже только с ролью ADMIN
    @PostMapping
    public String userSave(
            @RequestParam String username,
            @RequestParam String userlastname,
            @RequestParam String useradress,
            @RequestParam String userphonenumber,
            @RequestParam String password,
            @RequestParam Map<String, String> form,
            @RequestParam("userId") User user
    ) {
        userService.saveUser(user, username,userlastname,useradress, userphonenumber, password, form );
        return "redirect:/user";
    }

    // ПОЛЬЗОВАТЕЛЬ просматривает свои данные
    @GetMapping ("profile")
    public String getProfile (
            @AuthenticationPrincipal User user,
            Model model
    ) {
        model.addAttribute("user", user);
        model.addAttribute("email", user.getEmail());
        return "profile";
    }

    // ПОЛЬЗОВАТЕЛЬ меняет свои данные
    @PostMapping("profile")
    public  String updateProfile (
            @AuthenticationPrincipal User user,
            @RequestParam String password,
            @RequestParam String email,
            @RequestParam String userlastname,
            @RequestParam String useradress,
            @RequestParam String userphonenumber
    ) {
        userService.editUser(user, password, email, userlastname, useradress,userphonenumber);
        return "redirect:/user/profile";

    }

    @PreAuthorize("hasAuthority('ADMIN')") // дает допуск ко всем мапингам ниже только с ролью ADMIN
    @GetMapping("/delete/{user}") // ожидаем maping помимо /user через / будет идти идентификатор
    public String userDelete (
            @PathVariable User user,
            Model model
    ) {
        model.addAttribute("user", user);
        return "userDelete";
    }

    @PreAuthorize("hasAuthority('ADMIN')") // дает допуск ко всем мапингам ниже только с ролью ADMIN
    @PostMapping("/delete/{id}")
    public String userDelete (
            @RequestParam ("id") Long id,
            @RequestParam ("id") User user
    ) {
        //Long idLong = Long.parseLong(id);
        userService.userDelete(id, user);

        return "redirect:/user";
    }
}
