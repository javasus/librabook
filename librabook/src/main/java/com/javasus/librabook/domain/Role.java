package com.javasus.librabook.domain;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by javasus on 04.02.2020.
 */
public enum  Role implements GrantedAuthority{
    USER, ADMIN;

    @Override
    public String getAuthority() {
        return name();
    }
}
