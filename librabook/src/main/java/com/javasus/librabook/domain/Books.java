package com.javasus.librabook.domain;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * Created by javasus on 27.01.2020.
 */
@Entity  // This tells Hibernate to make a table out of this class
public class Books {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @NotBlank(message = "Please fill the book name")
    @Length(max = 255, message = "Text to long")
    private String namebook;
    @NotBlank(message = "Please fill the author")
    @Length(max = 255, message = "Text to long")
    private String authorbook;
    @NotBlank(message = "Please fill the publish")
    @Length(max = 255, message = "Text to long")
    private String publishbook;

    private String placebook;
    private Boolean issuebook = false;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User author;

    private String filename;

    private String filenameBook;


    public Books() {
    }

    public Books(String namebook, String authorbook, String publishbook, String placebook, Boolean issuebook, User user) {

        this.author = user;
        this.namebook = namebook;
        this.authorbook = authorbook;
        this.publishbook = publishbook;
        this.placebook = placebook;
        this.issuebook = issuebook;
    }

    public String getAuhtorName() {
        return author != null ? author.getUsername() : "<none>";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamebook() {
        return namebook;
    }

    public void setNamebook(String namebook) {
        this.namebook = namebook;
    }

    public String getAuthorbook() {
        return authorbook;
    }

    public void setAuthorbook(String authorbook) {
        this.authorbook = authorbook;
    }

    public String getPublishbook() {
        return publishbook;
    }

    public void setPublishbook(String publishbook) {
        this.publishbook = publishbook;
    }

    public String getPlacebook() {
        return placebook;
    }

    public void setPlacebook(String placebook) {
        this.placebook = placebook;
    }

    public Boolean getIssuebook() {
        return issuebook;
    }

    public void setIssuebook(Boolean issuebook) {
        this.issuebook = issuebook;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilenameBook() {
        return filenameBook;
    }

    public void setFilenameBook(String filenameBook) {
        this.filenameBook = filenameBook;
    }
}
