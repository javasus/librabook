package com.javasus.librabook.config;

/**
 * Created by javasus on 04.02.2020.
 */
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
    @Value("${upload.path}")
    private String uploadPath;

    @Value("${upload.pathBook}")
    private String uploadPathBook;

    //The configuration of our WEB layer
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //каждое обращение к серверу по пути /img/** с какими-то данными будет перенаправлять все
        //запросы по пути "file://" + uploadPath + "/"
        registry.addResourceHandler("/img/**")
                .addResourceLocations("file://" + uploadPath + "/");

        registry.addResourceHandler("/book/download/**")
                .addResourceLocations("file://" + uploadPathBook + "/");

        registry.addResourceHandler("/static/**")
                .addResourceLocations("classpath:/static/");
    }
}