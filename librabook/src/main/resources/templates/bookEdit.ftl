<#import "parts/common.ftl" as c>

<@c.page>
    <h4>Book editor</h4>
    <form enctype="multipart/form-data" action="/book/${book.id}" method="post">
        <input type="hidden" value="${book.id}" name="id" />
        <input type="hidden" value="${_csrf.token}" name="_csrf /">

        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Name book</label>
                <input type="text" class="form-control ${(namebookError??)?string('is-invalid', '')}"
                       value="<#if book??>${book.namebook}</#if>" name="namebook" />
                <#if namebookError??>
                    <div class="invalid-feedback">
                        ${namebookError}
                    </div>
                </#if>
            </div>
            <div class="form-group col-md-6">
                <label>Author</label>
                <input type="text" class="form-control ${(authorbookError??)?string('is-invalid', '')}" name="authorbook"
                       value="<#if book??>${book.authorbook}</#if>"/>
                <#if authorbookError??>
                    <div class="invalid-feedback">
                        ${authorbookError}
                    </div>
                </#if>
            </div>
        </div>

        <div class="form-group">
            <label>Publish</label>
            <input type="text" class="form-control ${(publishbookError??)?string('is-invalid', '')}" name="publishbook"
                   value="<#if book??>${book.publishbook}</#if>" />
            <#if publishbookError??>
                <div class="invalid-feedback">
                    ${publishbookError}
                </div>
            </#if>
        </div>

        <div class="form-group">
            <label>Storage</label>
            <input type="text" class="form-control" name="placebook" value="${book.placebook}" />
        </div>

        <div class="form-group">
            <label>Auhtor</label>
            <input type="text" class="form-control" name="auhtorName" value="${book.auhtorName}" />
        </div>


        <table class="table">
            <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <tr align="center">
                    <th>
                        <div class="my-1">
                            <p>путь к обложке</p>
                        </div>
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile01" name="file" />
                                <label class="custom-file-label" for="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                                    <#if book.filename??>
                                        ${book.filename}
                                    <#else>
                                        Файл не найден
                                    </#if>
                                </label>

                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                            </div>
                        </div>

                        <div class="my-1">
                            <p>путь к книге</p>
                        </div>
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile02" name="fileBook" />
                                <label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFileAddon02">
                                    <#if book.filenameBook??>
                                        ${book.filenameBook}
                                        <#else>
                                            Файл не найден
                                    </#if>
                                </label>

                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text" id="inputGroupFileAddon02">Upload</span>
                            </div>
                        </div>
                    </th>
                    <th width="200">
                        <#if book.filename??>
                            <img src="/img/${book.filename}" class="img-thumbnail">
                            <#else>
                                изображение не найдено
                        </#if>
                    </th>
                </tr>
            </tbody>
        </table>

        <div class="form-group">
            <div class="form-check">
                <input type="checkbox" name="issuebook" <#if book.issuebook?c=="true">checked="true"</#if>>
                <label class="form-check-label">
                    <p><strong>книга в наличии</strong></p>
                </label>
            </div>
        </div>

        <button class="btn btn-outline-dark" type="submit">Change</button>
        </form>

</@c.page>
