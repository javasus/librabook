<#import "parts/common.ftl" as c>

<@c.page>
    <h4>User editor
        <small class="text-muted ml-4">Не забудь сохранить данные.</small>
    </h4>
    <form action="/user" method="post">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Name</label>
                <input type="text" class="form-control" name="username" value="${user.username}" />
            </div>

            <div class="form-group col-md-6">
                <label>Last Name</label>
                <input type="text" class="form-control" name="userlastname" value="${user.userlastname!''}" />
            </div>
        </div>

        <div class="form-group ">
            <label>Adress</label>
            <input type="text" class="form-control" name="useradress" value="${user.useradress!''}"/>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Phone number</label>
                <input type="text" class="form-control" name="userphonenumber" value="${user.userphonenumber!'' }" />
            </div>

            <div class="form-group col-md-6">
                <label>Password</label>
                <input type="text" class="form-control" name="password" value="${user.password}" />
            </div>
        </div>

        <div class="my-2">
            <p><strong>Выберете роль пользователя :</strong></p>
        </div>
        <div class="form-group">
            <div class="form-check">
                <#list roles as role>
                    <div>
                        <label><input type="checkbox" class="form-check-input" name="${role}" ${user.roles?seq_contains(role)?string("cheked", "")}>${role}</label>
                    </div>
                </#list>
            </div>
        </div>
        <input type="hidden" value="${user.id}" name="userId" />
        <input type="hidden" value="${_csrf.token}" name="_csrf" />
        <button class="btn btn-outline-dark" type="submit">Save</button>
    </form>

</@c.page>
