<#import "parts/common.ftl" as c>

<@c.page>
    <h5><p class="text-center">Внимание вы удаляее книгу!</p></h5>

    <form method="post" action="/book/delete/id">
        <input type="hidden" value="${book.id}" name="id" />
        <input type="hidden" value="${_csrf.token}" name="_csrf" />

        <table class="table table-borderless">
            <thead>
            <tr align="center">
                <th scope="col">Name book</th>
                <th scope="col">Author</th>
                <th scope="col">Publish</th>
                <th scope="col">Cover</th>
            </tr>
            </thead>
            <tbody>
            <tr align="center">
                <td>${book.namebook}</td>
                <td>${book.authorbook}</td>
                <td>${book.publishbook}</td>
                <td>
                    <#if book.filename??>
                        <img src="/img/${book.filename}" class="img-thumbnail"  width="200">
                    </#if>
                </td>
            </tr>
            </tbody>
        </table>

        <button class="btn btn-danger btn-lg btn-block" type="submit">delete</button>
    </form>

</@c.page>