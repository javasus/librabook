<#import "parts/common.ftl" as c>


<@c.page>
    <form method="get" action="/main">
        <div class="form-group">
            <label><h5>Filter</h5></label>
            <input type="text" class="form-control" name="filternamebook" placeholder="search here" value="${filter?ifExists}"/>
        </div>

        <div class="form-group">
            <select name="filter" class="form-control" id="filter book">
                <option value="name">Name book</option>
                <option value="author">Author book</option>
                <option value="publish">Publish</option>
                <option value="place">Place</option>
                <option value="issue">Issue</option>
            </select>
        </div>
        <button type="submit" class="btn btn-outline-dark mb-4">search</button>
    </form>

    <table class="table table-striped">

        <thead>
        <tr align="center">
            <th scope="col">Name book</th>
            <th scope="col">Author</th>
            <th scope="col">Publish</th>
            <th scope="col">Place</th>
            <th scope="col">issue</th>
            <th scope="col">Cover</th>
            <th scope="col"></th>
        </tr>
        </thead>

        <tbody>
            <#list books as book>
                <tr align="center">
                    <th>${book.namebook}</th>
                    <td>${book.authorbook}</td>
                    <td>${book.publishbook}</td>
                    <td>${book.placebook}</td>
                    <td>
                        <#if book.issuebook = true>
                            available
                            <#else>
                                not available
                        </#if>
                    </td>
                    <td>
                        <#if book.filename??>
                            <img src="/img/${book.filename}" width="200" class="img-thumbnail" />
                            <#else>
                                no cover
                        </#if>
                    </td>
                    <td>
                        <#if book.filenameBook??>
                            <a class="btn btn-secondary" href="/book/download/${book.filenameBook}" download>dowload</a>
                        <#else>
                            <button type="button" class="btn btn-secondary" disabled>dowload</button>
                        </#if>
                    </td>
                </tr>
                <#else>
                    NO BOOKS
                </>
            </#list>
        </tbody>
    </table>
</@c.page>