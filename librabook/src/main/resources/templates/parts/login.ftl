<#macro login path>
    <form action="${path}" method="post">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">User Name</label>
            <div class="col-sm-6">
                <input type="text" name="username" class="form-control" placeholder="User name"/>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-6">
                <input type="password" name="password" class="form-control" placeholder="Password"/>
            </div>
        </div>
        <input type="hidden" name="_csrf" value="${_csrf.token}"/>
        <button class="btn btn-primary" type="submit"/>Sign In</button>
        <a class="btn btn-outline-dark ml-3" href="/registration">Registration</a>

    </form>
</#macro>

<#macro logout>
    <form action="/logout" method="post">
        <input type="hidden" name="_csrf" value="${_csrf.token}"/>
        <button class="btn btn-outline-secondary" type="submit">Sign Out</button>
    </form>
</#macro>

<#macro registration path>
    <form action="${path}" method="post">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Name </label>
                <input type="text" class="form-control ${(usernameError??)?string('is-invalid', '')}" name="username" placeholder="Name"
                value="<#if user??>${user.username}</#if>"/>
                <#if usernameError??>
                    <div class="invalid-feedback">
                        ${usernameError}
                    </div>
                </#if>
            </div>

            <div class="form-group col-md-6">
                <label>Last Name</label>
                <input type="text" class="form-control" name="userlastname" placeholder="Lastname"/>
            </div>
        </div>

        <div class="form-group">
            <label>Adress</label>
            <input type="text" class="form-control" name="useradress" placeholder="Adress"/>
        </div>

        <div class="form-group">
            <label>Phone number</label>
            <input type="text" class="form-control" name="userphonenumber" placeholder="Phone number"/>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Password</label>
                <input type="password" class="form-control ${(passwordError??)?string('is-invalid', '')}" name="password" placeholder="password"/>
                <#if passwordError??>
                    <div class="invalid-feedback">
                        ${passwordError}
                    </div>
                </#if>
            </div>

            <div class="form-group col-md-6">
                <label>Password</label>
                <input type="password" class="form-control ${(password2Error??)?string('is-invalid', '')}" name="password2" placeholder="retype password"/>
                <#if password2Error??>
                    <div class="invalid-feedback">
                        ${password2Error}
                    </div>
                </#if>
            </div>
        </div>

        <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control ${(emailError??)?string('is-invalid', '')}" name="email" placeholder="email"
                   value="<#if user??>${user.email}</#if>"/>
            <#if emailError??>
                <div class="invalid-feedback">
                    ${emailError}
                </div>
            </#if>
        </div>


        <input type="hidden" name="_csrf" value="${_csrf.token}"/>
        <button class="btn btn-outline-dark"  type="submit">To register</button>
    </form>
</#macro>

