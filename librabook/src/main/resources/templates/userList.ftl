<#import "parts/common.ftl" as c>

<@c.page>
    <h4>Users list
        <small class="text-muted ml-4">Тут вы можете изменить данные пользователей.</small>
    </h4>
    <table class="table table-striped-md">
        <thead>
            <tr align="center">
                <th scope="col">Name</th>
                <th scope="col">Role</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
        </thead>
        <tbody>
            <#list users as user>
                <tr align="center">
                    <td>${user.username}</td>
                    <td><#list user.roles as role>${role}<#sep>, </#list></td>
                    <td><a class="btn btn-outline-dark" href="/user/${user.id}">edit</a></td>
                    <td><a class="btn btn-danger" href="/user/delete/${user.id}">Delete</a></td>
                </tr>
            </#list>
        </tbody>
    </table>
</@c.page>