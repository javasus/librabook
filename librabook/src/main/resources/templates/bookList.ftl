<#import "parts/common.ftl" as c>

<@c.page>
    <button class="btn btn-outline-dark mb-4" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
        Add new book
    </button>
    <div class="collapse mb-4 <#if book??>show</#if>" id="collapseExample">
        <div class="card card-body">
            <form action="/book" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="post" />
                <input type="hidden" name="_csrf" value="${_csrf.token}"/>

                <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Book name</label>
                            <input type="text" class="form-control ${(namebookError??)?string('is-invalid', '')}"
                                   value="<#if book??>${book.namebook}</#if>" name="namebook" placeholder="введите название книги"/>
                            <#if namebookError??>
                                <div class="invalid-feedback">
                                    ${namebookError}
                                </div>
                            </#if>
                        </div>
                    <div class="form-group col-md-6">
                        <label>Author</label>
                        <input type="text" class="form-control ${(authorbookError??)?string('is-invalid', '')}"
                               value="<#if book??>${book.authorbook}</#if>" name="authorbook" placeholder="Ведите автора книги"/>
                        <#if authorbookError??>
                            <div class="invalid-feedback">
                                ${authorbookError}
                            </div>
                        </#if>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Publish</label>
                        <input type="text" class="form-control ${(publishbookError??)?string('is-invalid', '')}"
                               value="<#if book??>${book.publishbook}</#if>" name="publishbook" placeholder="Ведите издателя книги"/>
                        <#if publishbookError??>
                            <div class="invalid-feedback">
                                ${publishbookError}
                            </div>
                        </#if>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Storage</label>
                        <input type="text"class="form-control" name="placebook" placeholder="Ведите место хранения книги"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="issuebook"/>
                        <label class="form-check-label">
                            <p><strong>поставь галочку если книга в наличии</strong></p>
                        </label>
                    </div>
                </div>

                <div class="my-1">
                    <p> Прикрепите обложку книги</p>
                </div>
                <div class="input-group mb-3">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="inputGroupFile01" name="file">
                        <label class="custom-file-label" for="inputGroupFile01" aria-describedby="inputGroupFileAddon01">Choose file</label>
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                    </div>
                </div>

                <div class="my-1">
                    <p>Прикрепите книгу</p>
                </div>
                <div class="input-group mb-3">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="inputGroupFile02" name="fileBook">
                        <label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFileAddon02">Choose file</label>
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text" id="inputGroupFileAddon02">Upload</span>
                    </div>
                </div>

                <button class="btn btn-outline-dark" type="submit">Load</button>

            </form>
        </div>
    </div>

    <form action="/book" method="get">
        <div class="form-group">
            <label><h5>Filter</h5></label>
            <input type="text" class="form-control" name="filternamebook" placeholder="search here" value="${filter?ifExists}"/>
        </div>

        <div class="form-group">
            <select name="filter" class="form-control" id="filter book">
                <option value="name">Name book</option>
                <option value="author">Author book</option>
                <option value="publish">Publish</option>
                <option value="place">Place</option>
                <option value="issue">Issue</option>
            </select>
        </div>
        <button type="submit" class="btn btn-outline-dark mb-4">search</button>
    </form>


    <table class="table table-striped">

        <thead>
        <tr align="center">
            <th scope="col">Name book</th>
            <th scope="col">Author</th>
            <th scope="col">Publish</th>
            <th scope="col">Place</th>
            <th scope="col" nowrap>who added</th>
            <th scope="col">issue</th>
            <th scope="col">Cover</th>
            <th scope="col">download</th>
            <th></th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        <#list books as book>
            <tr align="center">
                <td>${book.namebook}</td>
                <td>${book.authorbook}</td>
                <td>${book.publishbook}</td>
                <td>${book.placebook}</td>
                <td>${book.auhtorName}</td>
                <td>
                    <#if book.issuebook??>
                        <#if book.issuebook = true>
                            available
                            <#else>
                                not available
                        </#if>
                    <#else>
                        ERROR
                    </#if>

                </td>
                <td>
                    <#if book.filename??>
                        <img src="/img/${book.filename}" class="img-thumbnail"/>
                    </#if>
                </td>
                <td>
                    <#if book.filenameBook??>
                        <a class="btn btn-secondary" href="/book/download/${book.filenameBook}" download>download</a>
                        <#else>
                            <button type="button" class="btn btn-secondary" disabled>dowload</button>
                    </#if>
                </td>
                <td><a class="btn btn-success" href="/book/${book.id}">edit</a></td>
                <td><a class="btn btn-danger" href="/book/delete/${book.id}">delete</a></td>
            </tr>
        </#list>
        </tbody>
    </table>

</@c.page>