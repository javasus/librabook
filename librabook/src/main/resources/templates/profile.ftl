<#import "parts/common.ftl" as c>
    <!--<#import "parts/login.ftl" as r>-->

<@c.page>
        <h5>${user.username}</h5>
        <form method="post">
            <div class="form-row">

                <div class="form-group col-md-6">
                    <label>Last Name</label>
                    <input type="text" class="form-control" name="userlastname" value="${user.userlastname!''}" />
                </div>
            </div>

            <div class="form-group ">
                <label>Adress</label>
                <input type="text" class="form-control" name="useradress" value="${user.useradress!''}">
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Phone number</label>
                    <input type="text" class="form-control" name="userphonenumber" value="${user.userphonenumber!''}" />
                </div>

                <div class="form-group col-md-6">
                    <label>Password</label>
                    <input type="text" class="form-control" name="password" value="Введите новый пароль для изменения пароля" />
                </div>

                <div class="form-group col-md-6">
                    <label>Email</label>
                    <input type="text" class="form-control" name="email" value="${user.email!''}" />
                </div>
            </div>

            <input type="hidden" value="${user.id}" name="userId" />
            <input type="hidden" value="${_csrf.token}" name="_csrf" />
            <button class="btn btn-outline-dark" type="submit">Save</button>
        </form>
</@c.page>