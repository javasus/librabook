<#import "parts/common.ftl" as c>

<@c.page>
    <h5><p class="text-center">Внимание, вы удаляее пользователя!</p></h5>

    <form method="post" action="/user/delete/id">
        <input type="hidden" value="${user.id}" name="id" />
        <input type="hidden" value="${_csrf.token}" name="_csrf" />

        <table class="table table-borderless">
            <thead>
            <tr align="center">
                <th scope="col">Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Adress</th>
                <th scope="col">Phone number</th>
                <th scope="col">Email</th>
            </tr>
            </thead>
            <tbody>
            <tr align="center">
                <td>${user.username}</td>
                <td>${user.userlastname!''}</td>
                <td>${user.useradress!''}</td>
                <td>${user.userphonenumber!''}</td>
                <td>${user.email!''}</td>
            </tr>
            </tbody>
        </table>

        <button class="btn btn-danger btn-lg btn-block" type="submit">delete</button>
    </form>
</@c.page>
