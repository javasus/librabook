insert into usr (id, username, password, active)
    values (0, 'admin', '$2a$08$gGlhV.nk9TyVJ7L7ePZM9uTOi2O574vfhy42OhwGJrxHfNSNxcGTa', true);

insert into user_role (user_id, roles)
    values (0, 'USER'), (0, 'ADMIN');