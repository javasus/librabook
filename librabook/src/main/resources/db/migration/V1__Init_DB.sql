create table books (
    id bigint not null,
    authorbook varchar(255),
    filename varchar(255),
    filename_book varchar(255),
    issuebook bit,
    namebook varchar(255) not null,
    placebook varchar(255),
    publishbook varchar(255),
    user_id bigint,
    primary key (id)
);

create table hibernate_sequence (next_val bigint);

insert into hibernate_sequence values ( 1 );

insert into hibernate_sequence values ( 1 );

create table user_role (
    user_id bigint not null,
    roles varchar(255)
);

create table usr (
    id bigint not null,
    activation_code varchar(255),
    active bit not null,
    email varchar(255),
    password varchar(255) not null,
    useradress varchar(255),
    userlastname varchar(255),
    username varchar(255) not null,
    userphonenumber varchar(255),
    primary key (id)
);

alter table books
    add constraint books_user_fk
    foreign key (user_id) references usr (id);

alter table user_role
    add constraint user_role_user_fk
    foreign key (user_id) references usr (id);