package com.javasus.librabook;

import com.javasus.librabook.controller.MainController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

/**
 * Created by javasus on 01.03.2021.
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-dev.properties")
@WithUserDetails("Roman") // добавли, что бы заходить в приложение под  существующим авторизованным пользоватлем.
public class MainControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired // подключаем наш контроллер
    private MainController controller;

    @Test
    public void mainPageTest() throws Exception {
        this.mockMvc.perform(get("/main")) //делаем get завпрос на страницу "/main"
                .andDo(print())
                // добавили анотацию - @WithUserDetails("Roman"), что бы  зайти на страницу под существующим авторизованным пользоватлем.
                .andExpect(authenticated()) // метод - authenticated, проверяет, что пользователь был корректно аутентифицирован.
                // с помощью xpath ищем на странице элемент показывающий что мы зашли под существующим пользователем  и проверяем , что в этом элементе содержиться
                // текстовое занчение - Roman.
                .andExpect(xpath("//*[@id='navbarSupportedContent']/div").string("Roman"));
    }
}
