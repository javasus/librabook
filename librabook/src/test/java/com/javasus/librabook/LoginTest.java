package com.javasus.librabook;


import com.javasus.librabook.controller.MainController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-dev.properties")
public class LoginTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired // подключаем наш контроллер
    private MainController controller;

    @Test
    public void test() throws  Exception {
        assertThat(controller).isNotNull(); //assertThat - проверяет условие, в данном случае проверяет не пустой ли controller.
    }

    @Test
    public void contextLoads() throws Exception {
                    this.mockMvc.perform(get("/")) // совершаем get запрос на главную страницу приложения
                    .andDo(print()) //выводим в консоль полученный результат.
                    .andExpect(status().isOk())  //Проверяем отклик загружемой страницы (Если код 200 значит все ок!) andExpect это обертка над  assertThat, позволяет сравнить результат который возвращаеться тестируемым кодом с результатом который мы ожидаем.
                            // Проверяем, что страница содержит  указанный в кавычках текст.
                    .andExpect(content().string(containsString("Hello, guest!")))  //Проверяем, что стартовая страница содержит приветсвие.
                    .andExpect(content().string(containsString("unknown"))); // Проверяем, что на стартовой странице пользователь не известен. Проверяем, что страница содержит  указанный в кавычках текст.
    }
    @Test
    public void accessDeniedTest() throws Exception {
        this.mockMvc.perform(get("/main"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("http://localhost/login"));
    }
    @Test
    public void correctLoginTest() throws Exception {
        // Добавили специальное dependency - spring-security-test, что бы задействовать  метод formLogin.
        //даный метод смотри как мы в контексте определиили login page и вызывает обращение к этой странице.
        this.mockMvc.perform(formLogin().user("Roman").password("123")) //заходим под известнымнам пользователем
                .andDo(print())
                .andExpect(status().is3xxRedirection()) //ожидаем статус redirection
                .andExpect(redirectedUrl("/")); // проверяем переходит ли на  главную страницу.
    }

    @Test
    public void badCredentials() throws Exception {
        // import method  - MockMvcRequestBuilders.post
        //Пробуем на странице login зайти под несуществующим пользователем
        this.mockMvc.perform(post("/login").param("user", "Koala"))
                .andDo(print())
                .andExpect(status().isForbidden()); // ожидаем статус Forbidden (запрещенно).
    }


}
